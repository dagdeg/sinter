

module.exports.sendWelcomeMail = function(obj) {
  sails.hooks.email.send(
    "testEmail",
    {
      Name: obj.name,
    },
    {
      to: obj.email,
    },
    function(err) {
      if(err) {
        console.log(err);
      }
      else {
        console.log("Email to "+obj.email+" has been sent successfully!");
      }
    }
  )
};
