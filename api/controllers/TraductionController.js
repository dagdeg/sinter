/**
 * TraductionController
 *
 * @description :: Server-side logic for managing Traductions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  create: function (req, res) {
    console.log(req.param("traduction", "ala"));
    if(req.method=="POST" && req.param("traduction",null)!=null)
    {
      Traduction.create(req.param("traduction")).exec(function(err,model){
        if (err) {
          res.view("500")
        }else {
          Mailer.sendWelcomeMail({'name':"ala",'email':"aladogui@gmail.com"});
          res.view('template/traduction')
        }
      });

    }
  else
    {
      res.view('template/traduction');
    }
  },

  send: function(req, res) {
    var params = _.extend(req.query || {}, req.params || {}, req.body || {});

    Traduction.create(params, function userCreated (err, createdUser) {

      if (err) return res.send(err,500);

      sails.hooks.email.send(
        "testEmail", createdUser , {
          to: "aladogui@gmail.com",
          subject: "Nouvelle demande de traduction"
        },
        function(err) {
          if (err) {return res.serverError(err);}

          return res.ok();
        }
      );
      res.view('template/sentEmail');
    });

  },



  traductionMachin : function (req,res) {
    res.view('template/traductionMachin');

  },
  machin : function (req,res) {
    res.redirect("/template/indexMachin");
  },

};

